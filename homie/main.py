#!/usr/bin/env python3

import datetime
import time
from homie import settings
from homie import setup


class Main(object):
    def __init__(self, args):
        self.args = settings.Arguments().args
        self.settings = settings.Settings(args.config)
        self.temperature_sensors, self.bool_sensors, self.bool_actors, self.heat_controls, self.db = setup.Setup(self.settings).setup()

    def do(self):
        self.temperature_sensors.update_all()
        self.bool_sensors.update_all()
        self.bool_actors.update_all()
        self.heat_controls.update_all()
        for sensor_list in [self.temperature_sensors, self.bool_sensors, self.bool_actors]:
            for sensor in sensor_list:
                self.db.put(sensor.time, 'onewire', (sensor.address, sensor.endpoint, sensor.value))

    def run(self):
        interval = 5
        while True:
            time.sleep(interval - datetime.datetime.now().second%interval)
            self.do()
