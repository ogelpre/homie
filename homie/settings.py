import argparse
import yaml


class Arguments(object):
    def __init__(self):
        parser = argparse.ArgumentParser(description='simple heat controller')

        parser.add_argument('--config', '-c', **self.helptext_default('config', 'config.yml'))

        self.parser = parser

    @staticmethod
    def helptext_default(text, default):
        help_text = "{text} (default: {default})".format(text=text, default=default)
        return {'default': default, 'help': help_text}

    @property
    def args(self):
        args = self.parser.parse_args()

        return args


class Settings(dict):
    def __init__(self, path, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with open(path) as stream:
            self.update(yaml.load(stream))
