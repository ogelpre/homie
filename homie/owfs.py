import os


class Proxy(object):
    def __init__(self, base_path='/mnt/owfs'):
        self.base_path = base_path

    def path(self, address, endpoint, uncached):
        if uncached:
            return os.path.join(self.base_path, 'uncached', address, endpoint)
        else:
            return os.path.join(self.base_path, address, endpoint)

    def read(self, address, endpoint, uncached=False):
        with open(self.path(address, endpoint, uncached), 'r') as stream:
            return stream.read()

    def write(self, address, endpoint, data, uncached=False):
        with open(self.path(address, endpoint, uncached), 'w') as stream:
            stream.write(data)
            stream.flush()
