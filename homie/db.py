import psycopg2
import datetime
import time
import multiprocessing

class PostgresDB(object):
    def __init__(self, dsn):
        self.__dsn = dsn
        self.__connection = None

    def __execute(self, query, vars=None):
        connection = self.connection
        cursor = connection.cursor()
        cursor.execute(query, vars)
        connection.commit()
        del cursor

    def execute(self, query, vars=None):
        while True:
            try:
                self.__execute(query, vars)
                break
            except psycopg2.OperationalError:
                time.sleep(1)
            except psycopg2.InterfaceError:
                time.sleep(1)

    @property
    def connection(self):
        try:
            self.__connection.cursor().execute('SELECT NULL;')
        except AttributeError:
            self.__connection = psycopg2.connect(self.__dsn)
        except psycopg2.OperationalError:
            self.__connection = psycopg2.connect(self.__dsn)
        except psycopg2.InterfaceError:
            time.sleep(5)
            self.__connection = psycopg2.connect(self.__dsn)
        return self.__connection


class DB(object):
    def __init__(self, dsn):
        self.__db = PostgresDB(dsn)
        self.__queue = multiprocessing.Queue()

        self.__db_process = multiprocessing.Process(target=self.db_process_f, args=(self.__db, self.__queue,))
        self.__db_process.start()

    def put(self, time, source, data):
        self.__queue.put((time, source, data))

    @staticmethod
    def db_process_f(db, queue):
        while True:
            time, source, data = queue.get()
            if source == 'onewire':
                address = data[0]
                endpoint = data[1]
                value = data[2]
                if address.startswith('28'):
                    db.execute("INSERT INTO onewire_temperature (time, address, endpoint, value) VALUES (%s, %s, %s, %s);", (time, address, endpoint, value))
                elif address.startswith('29'):
                    db.execute("INSERT INTO onewire_bool (time, address, endpoint, value) VALUES (%s, %s, %s, %s);", (time, address, endpoint, value))
