import datetime


class HeatControl(object):
    def __init__(self, identifier, name, enabled, temperature_sensor, heater, window, schedule):
        self.identifier = identifier
        self.name = name
        self.enabled = enabled
        self.temperature_sensor = temperature_sensor
        self.heater = heater
        self.window = window
        self.schedule = schedule
        self.on_by_freeze_protection = False

    def __repr__(self):
        return "{}, identifier: {}".format(type(self).__name__, self.identifier)

    def update(self):
        safe_temperature = 16
        if self.window.value and self.temperature_sensor.value < safe_temperature - 0.25:
            self.heater.write(True)
            self.on_by_freeze_protection = True
            return
        elif (self.window.value and self.temperature_sensor.value >= safe_temperature and self.on_by_freeze_protection or
                not self.window.value and self.on_by_freeze_protection):
            self.heater.write(False)
            self.on_by_freeze_protection = False
            return
        elif self.on_by_freeze_protection:
            return
        if not self.enabled:
            return
        time = Time()
        _, set_temperature = self.schedule[time]
        if (not self.window.value or
                set_temperature is None or
                self.temperature_sensor.value is None or
                self.temperature_sensor.value >= set_temperature):
            self.heater.write(False)
        elif self.temperature_sensor.value <= set_temperature - 0.25:
            self.heater.write(True)

class HeatControlList(object):
    def __init__(self):
        self.heat_controls = list()

    def __iter__(self):
        return self.heat_controls.__iter__()

    def append(self, heat_control):
        self.heat_controls.append(heat_control)

    def update_all(self):
        for heat_control in self.heat_controls:
            heat_control.update()


class Schedule(object):
    def __init__(self):
        self.schedule = list()

    def __getitem__(self, key):
        schedule = sorted(self.schedule)
        for lower, upper in zip(schedule[:], schedule[1:]):
            if lower[0] <= key < upper[0]:
                return lower
        if schedule[-1][0] <= key or key < schedule[0][0]:
            return schedule[-1]
        else:
            raise ValueError

    def add_date(self, time, temperature):
        self.schedule.append((Time(*time.split(':')), int(temperature)))


class Time(object):
    def __init__(self, hour=None, minute=None):
        if hour is None or minute is None:
            date = datetime.datetime.now()
        if hour is None:
            self.hour = date.hour
        else:
            self.hour = self.sanitize_hour(hour)
        if minute is None:
            self.minute = date.minute
        else:
            self.minute = self.sanitize_minute(minute)

    def __lt__(self, other):
        if self.hour < other.hour:
            return True
        elif self.hour == other.hour and self.minute < other.minute:
            return True
        else:
            return False

    def __eq__(self, other):
        return self.hour == other.hour and self.minute == other.minute

    def __le__(self, other):
        return self.__lt__(other) or self.__eq__(other)

    def __repr__(self):
        return "{}:{}".format(self.hour, self.minute)

    @staticmethod
    def sanitize_hour(hour):
        h = int(hour)
        if 0 <= h <= 23:
            return h
        else:
            ValueError

    @staticmethod
    def sanitize_minute(minute):
        m = int(minute)
        if 0 <= m <= 59:
            return m
        else:
            ValueError
