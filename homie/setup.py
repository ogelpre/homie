from homie import onewire
from homie import heat_control
from homie import db

class Setup(object):
    def __init__(self, settings):
        self.settings = settings
        self.temperature_sensors = onewire.TemperatureSensorList()
        self.bool_sensors = onewire.BoolSensorList()
        self.bool_actors = onewire.BoolActorList()
        self.heat_controls = heat_control.HeatControlList()
        self.db = None

    def setup(self):
        self._setup_1wire()
        self._setup_heat_control()
        self._setup_db()
        return self.temperature_sensors, self.bool_sensors, self.bool_actors, self.heat_controls, self.db

    def _setup_1wire(self):
        for bus_member in self.settings['1wire']:
            if bus_member['address'].startswith('28.'):
                self.temperature_sensors.append(onewire.TemperatureSensor(bus_member['id'], bus_member['name'], bus_member['address']))
            elif bus_member['address'].startswith('29.')  and not bus_member['write']:
                self.bool_sensors.append(onewire.BoolSensor(bus_member['id'], bus_member['name'], bus_member['address'], bus_member['port']))
            elif bus_member['address'].startswith('29.') and bus_member['write']:
                self.bool_actors.append(onewire.BoolActor(bus_member['id'], bus_member['name'], bus_member['address'], bus_member['port']))
            else:
                raise ValueError("unknown 1-wire sensor {}".format(bus_member['address']))

    def _setup_heat_control(self):
        for hc in self.settings['heat_control']:
            temperature_sensor = self.temperature_sensors[hc['temperature_sensor']]
            heater = self.bool_actors[hc['heater']]
            window = self.bool_sensors[hc['window']]
            schedule = heat_control.Schedule()
            for date in hc['schedule']:
                schedule.add_date(date['time'], date['temperature'])
            self.heat_controls.append(heat_control.HeatControl(hc['id'], hc['name'], hc['enabled'], temperature_sensor, heater, window, schedule))

    def _setup_db(self):
        self.db = db.DB(self.settings['db']['dsn'])
