import datetime
from homie import owfs

proxy = owfs.Proxy()


class Sensor(object):
    def __init__(self, identifier, name, address, endpoint):
        self.identifier = identifier
        self.name = name
        self.address = address
        self.endpoint = endpoint
        self.__value = None
        self.__time = None

    def __repr__(self):
        return "{}, identifier: {}, value: {}".format(type(self).__name__, self.identifier, self.__value)

    @property
    def value(self):
        if self.__value is None:
            self.update()
        return self.__value

    def update(self):
        self.__value = self.read()
        self.__time = datetime.datetime.now()

    def read(self, uncached=False):
        return self.read_filter(proxy.read(self.address, self.endpoint, uncached))

    @property
    def time(self):
        return self.__time

    @staticmethod
    def read_filter(value):
        return value


class SensorList(object):
    def __init__(self):
        self.sensors = list()

    def __iter__(self):
        return self.sensors.__iter__()

    def __getitem__(self, key):
        return next((sensor for sensor in self.sensors if sensor.identifier == key), None)

    def append(self, sensor):
        self.sensors.append(sensor)

    def update_all(self):
        self.pre_update_all()
        for sensor in self.sensors:
            sensor.update()

    def pre_update_all(self):
        pass


class TemperatureSensor(Sensor):
    def __init__(self, identifier, name, address, endpoint='temperature'):
        self.read_filter = float
        super().__init__(identifier, name, address, endpoint)


class TemperatureSensorList(SensorList):
    def pre_update_all(self):
        proxy.write('simultaneous', 'temperature', '1')


class BoolSensor(Sensor):
    def __init__(self, identifier, name, address, endpoint):
        super().__init__(identifier, name, address, endpoint)

    @staticmethod
    def read_filter(value):
        if value == "0":
            return False
        elif value == "1":
            return True
        else:
            raise ValueError


class BoolSensorList(SensorList):
    pass


class Actor(Sensor):
    def write(self, data, uncached=False, force=False):
        new_data = self.write_filter(data)
        if force or self.write_filter(self.value) != new_data:
            proxy.write(self.address, self.endpoint, new_data, uncached)
            self.__value = self.read_filter(new_data)
            self.__time = datetime.datetime.now()

    @staticmethod
    def write_filter(value):
        return value


class ActorList(SensorList):
    pass


class BoolActor(Actor):
    def __init__(self, identifier, name, address, endpoint):
        super().__init__(identifier, name, address, endpoint)

    @staticmethod
    def read_filter(value):
        if value == "0":
            return False
        elif value == "1":
            return True
        else:
            raise ValueError

    @staticmethod
    def write_filter(value):
        return ('0', '1')[value]


class BoolActorList(ActorList):
    pass
