#!/usr/bin/env python3

import homie

if __name__ == '__main__':
    args = homie.settings.Arguments().args
    main = homie.main.Main(args)
    main.run()
